#include <iostream>
#include <cstdlib>
#include <math.h>
#include "main.h"

using namespace std;

void devineNombre(){
    cout << "Quel est mon nombre ? " << endl;
    int nombre = rand() % 1000 +1;
    int nb = -1;
    int i =0;

    while (nb != nombre) {
       cin >> nb;
       if (nb > nombre) {
           cout << "Le nombre à trouver est plus petit " << endl;
       }
       else if (nb < nombre) {
           cout << "Le nombre à trouver est plus grand " << endl;
       }
       i ++;
    }
    cout << "Félicitations ! Vous avez trouvez le nombre en " << i << " coup(s). " << endl;
}

void devineNombreOrdinateur(){
    cout << "Quel est mon nombre ? " << endl;
    int nombre;
    cin >> nombre;

    int min = 0;
    int max =1000;
    int nbPropose = -1;

    while (nbPropose != nombre) {
        nbPropose = rint((min+max) /2);
        cout << nbPropose << endl;
        if (nbPropose < nombre){
            min = nbPropose;
            cout << "Le nombre à trouver est plus grand"  << endl;
        }
        else if(nbPropose > nombre){
            max = nbPropose;
            cout << "Le nombre à trouver est plus petit"  << endl;
        }
    }
    cout << "Félicitations ! " << endl;
}

int main()
{
    //affichage du prenom
    char prenom[20];
    cout << "Quel est ton prénom ? " << endl;
    cin >> prenom;
    cout << "Salut " << prenom << endl;

    //affichage du prenom et du nom
    //bonus : les lettre en majuscule - la 1er lettre du prenom et le nom de famille en entier
    char pren[20];
    char nom[20];

    cout << "Quel est ton prénom et ton nom ? " << endl;
    cin >> pren >> nom;


    int i;
    for (i=0; i< 20; i++) {
        if (i == 0) {
            pren[i]=toupper(pren[i]);
        }
        else {
            pren[i]=tolower(pren[i]);
        }
    }

    int j;
    for(j =0; j< 20; j++) {
        nom[j]=toupper(nom[j]);
    }
    cout << "Salut " << pren << " " << nom << endl;

    //Nombre à trouver par l'utilisateur
    devineNombre();

    //Nombre à trouver par l'ordinateur selon la saisie d'un utilisateur
    devineNombreOrdinateur();

    return 0;
}
