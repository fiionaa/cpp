#include <iostream>
#include <cstdlib>
#include "main.h"

using namespace std;

string calculScore(int j1, int j2){
    int scoreJ1 = 0;
    int scoreJ2 = 0;
    bool endPartie1 = false;
    bool endPartie2 = false;

    for(int i = 1; i < j1+1; i++){
        if(i % 2 == 0){
            scoreJ1 +=30;
        }
        else if (i % 3 == 0){
            scoreJ1 +=40;
            endPartie1 = true;
        }
        else{
            scoreJ1 +=15;
        }
    }
    for(int i = 1; i < j2+1; i++){
        if(i % 2 == 0){
            scoreJ2 +=30;
        }
        else if (i % 3 == 0){
            scoreJ2 +=40;
            endPartie2 = true;
        }
        else{
            scoreJ2 +=15;
        }
    }
    if(endPartie1 || endPartie2){
        if(scoreJ1 == scoreJ2){
            return "égalité";
        }

    }else{
        if(scoreJ1 == scoreJ2){
            return to_string(scoreJ1)+"A";
        }
    }

    return "A-"+to_string(scoreJ1)+" B-"+to_string(scoreJ2);

}

int main()
{
    cout << calculScore(28,28) << endl;
    return 0;
}

