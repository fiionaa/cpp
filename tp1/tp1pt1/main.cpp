#include <iostream>
#include <cstdlib>
#include "main.h"

using namespace std;

int somme(int a, int b)
{
    return a+b;
}

void inverse(int* a, int* b)
{
    int tempb = *b;
    int tempa = *a;
    *a = tempb;
    *b = tempa;
}

void somme2(int& a, int& b, int& c)
{//référence
    c = a+b;
}

void somme2bis(int* a, int* b, int* c)
{//pointeur
    *c = *a + *b;
}

void afficherTableau(int tab[], int size){
    cout << "tab = [";
    for (int j =0; j < size; j++){
        if (j == size -1){
            cout << tab[j];
        }
        else {
            cout << tab[j] << "," ;
        }
    }
    cout << "]" << endl ;
}

int* tabtriecroissant(int tab[], int size) {

    int i =1;

    while (i< size){
        if (tab[i] < tab[i-1]){
            inverse(&tab[i], &tab[i-1]);
            i = 1;
        }else{
            i++;
        }
    }
    return tab;
}

int* tabtriedecroissant(int tab[], int size) {

    int i =1;

    while (i< size){
        if (tab[i] > tab[i-1]){
            inverse(&tab[i], &tab[i-1]);
            i = 1;
        }else{
            i++;
        }
    }
    return tab;
}

int* inverseTri(int tab[], int size){
    int i =0;
    int last = size -1;

    while( i < size){
        if(i != last && last > i){
            inverse(&tab[i],&tab[last]);
        }else{
            break;
        }
        i++;
        last = size - (i +1) ;
    }
    return tab;
}

int main()
{
    cout << "La somme est :" << somme(3,4) << endl;

    int a= 5; int b= 4;
    inverse(&a,&b);
    cout << "La valeur inversée de a qui est: " << a << " devient : " << b << endl;

    int c= 11; int d=7; int e=8;
    cout << "La valeur de e est : " << e << endl;
    somme2(c, d, e);
    cout << "La nouvelle valeur de e est : " << e << endl;


    int f= 11; int g=7; int h=8;
    cout << "La valeur de f est : " << f << endl;
    somme2bis(&f, &g, &h);
    cout << "La nouvelle valeur de e est : " << f << endl;


    int tabpositif[8];
    int const tailletab(8);

    int tailleTableauUtilisateur;
    int isCroissant;

    for (int j =0; j < tailletab; j++){
        tabpositif[j] = rand() %100;
    }

    afficherTableau(tabpositif, tailletab);

    int *tableau_trie;

    tableau_trie = tabtriecroissant(tabpositif, tailletab);

    cout << "Tableau trié" << endl;
    afficherTableau(tableau_trie,tailletab);

    cout << "Taille du tableau: " << endl;
    cin >> tailleTableauUtilisateur;
    cout << "1 pour un tri croissant, 0 pour un tri décroissant: " << endl;
    cin >> isCroissant;

    int tableauUtilisateur[tailleTableauUtilisateur];

    for(int i =0; i< tailleTableauUtilisateur; i++){
        tableauUtilisateur[i] = rand() % 100;
    }

    int* tabTrie;
    if(isCroissant == 1){
        tabTrie = tabtriecroissant(tableauUtilisateur, tailleTableauUtilisateur);
    }else{
        tabTrie = tabtriedecroissant(tableauUtilisateur, tailleTableauUtilisateur);
    }
    cout << "Tableau trié" << endl;
    afficherTableau(tabTrie, tailleTableauUtilisateur);

    cout << "Inverse du tri" << endl;
    int* tabTrieInverse;

    tabTrieInverse = inverseTri(tabTrie, tailleTableauUtilisateur);

    afficherTableau(tabTrieInverse, tailleTableauUtilisateur);
    return 0;

}
