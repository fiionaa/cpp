#ifndef GRILLE_H
#define GRILLE_H

#include <iostream>
#include <vector>

#include "case.h"
#include "diagonale.h"
#include "joueur.h"

class Grille
{
public:
    //autre méthodes
    void DeposerJeton(Joueur joueur, Case* c);
    virtual bool VictoireJoueur(Joueur joueur) = 0;
    void AfficherGrille();
    Case* GetCase(int x, int y) const;
    bool isPleine();
    int getNbLigne();
    int getNbColonne();
    virtual bool LigneComplete(Joueur joueur, int ligne) = 0;
    bool ColonneComplete(Joueur joueur, int colonne) const;
    bool DiagonaleComplete(Joueur joueur, Diagonale d) const;
    virtual Case* getCaseOfColonne(int colonne) = 0;

protected:
    vector<Case*> m_grille = vector<Case*>();
    int m_ligne;
    int m_colonne;
    vector<Diagonale> m_diagonales = vector<Diagonale>();
};

#endif // GRILLE_H
