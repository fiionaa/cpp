#include "jeu.h"

Jeu::Jeu(int choixJeu)
{
    if(choixJeu == 0){
        this->grille = new GrilleMorpion();
        lancerMorpion();
    }else{
        this->grille = new GrillePuissance4();
        lancerPuissance4();
    }
}

void Jeu::lancerMorpion(){

    Joueur joueur1 = *(new Joueur(1));
    Joueur joueur2 = *(new Joueur(2));

    Joueur prevJoueur = joueur2;

    bool partieTerminee = false;

    while(!partieTerminee){
        Joueur joueur = *(new Joueur(0));
        if(prevJoueur.getId() == 1){
            joueur = joueur2;
            cout << "Au tour du joueur 2" << endl;
        }else{
            joueur = joueur1;
            cout << "Au tour du joueur 1" << endl;
        }
        int x,y;

        cout << "Numero de la ligne" << endl;
        cin >> x;
        cout << endl;
        cout <<"Numero de la colonne" << endl;
        cin >> y;
        if(x >= this->grille->getNbLigne() || y >= this->grille->getNbColonne()){
            cout << "Coordonnees en dehors de la grille" << endl;
        }else{
            this->grille->DeposerJeton(joueur, this->grille->GetCase(x,y));
            this->grille->AfficherGrille();
            if(this->grille->VictoireJoueur(joueur)){
                cout << "Felicitations, vous avez gagne" << endl;
                partieTerminee = true;
            }else{
                if(this->grille->isPleine()){
                    cout << "Match nul. Tapez 1 pour recommencer une partie "<< endl;
                    int reponse;
                    cin >> reponse;
                    if(reponse == 1){
                        partieTerminee = true;
                        lancerMorpion();
                    }else{
                        cout << "Au revoir" << endl;
                        partieTerminee = true;
                    }
                }
            }
            prevJoueur = joueur;
        }

    }
}
void Jeu::lancerPuissance4(){
    Joueur joueur1 = *(new Joueur(1));
    Joueur joueur2 = *(new Joueur(2));

    Joueur prevJoueur = joueur2;

    bool partieTerminee = false;

    while(!partieTerminee){
        Joueur joueur = *(new Joueur(0));
        if(prevJoueur.getId() == 1){
            joueur = joueur2;
            cout << "Au tour du joueur 2" << endl;
        }else{
            joueur = joueur1;
            cout << "Au tour du joueur 1" << endl;
        }
        int y;

        cout <<"Numero de la colonne" << endl;
        cin >> y;
        cout << "Nombre de colonnes " << this->grille->getNbColonne() << endl;
        if(y >= this->grille->getNbColonne()){
            cout << "Coordonnees en dehors de la grille" << endl;
        }else{
            Case* case1 = this->grille->getCaseOfColonne(y);
            this->grille->DeposerJeton(joueur,case1);
            this->grille->AfficherGrille();
            if(this->grille->VictoireJoueur(joueur)){
                cout << "Felicitations, vous avez gagne" << endl;
                partieTerminee = true;
            }else{
                if(this->grille->isPleine()){
                    cout << "Match nul. Tapez 1 pour recommencer une partie "<< endl;
                    int reponse;
                    cin >> reponse;
                    if(reponse == 1){
                        partieTerminee = true;
                        lancerPuissance4();
                    }else{
                        cout << "Au revoir" << endl;
                        partieTerminee = true;
                    }
                }
            }
            prevJoueur = joueur;
        }

    }
}
