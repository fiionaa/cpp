#include "case.h"
using namespace std;

Case::Case(int m_x,int m_y)
{
    this->m_x = m_x;
    this->m_y = m_y;
}
bool Case::isVide()
{
    if(this->m_joueur.getId() != 0){
        return false;
    }else{
        return true;
    }
}
int Case::getX() const{
    return this->m_x;
}
int Case::getY() const{
    return this->m_y;
}
void Case::setJoueur(Joueur j){
    this->m_joueur = j;
}
bool Case::occupeParJoueur(Joueur j){
    if(this->m_joueur.getId() == j.getId()){
        return true;
    }
    return false;
}
Joueur Case::getJoueur(){
    return this->m_joueur;
}
