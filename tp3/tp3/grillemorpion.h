#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include <iostream>
#include <vector>

#include "grille.h"


class GrilleMorpion:public Grille
{
public:
    // Constructeurs
    GrilleMorpion();

    //autre méthodes
    bool LigneComplete(Joueur joueur, int ligne) override;
    bool VictoireJoueur(Joueur joueur) override;
    Case* getCaseOfColonne(int colonne) override;
};

#endif // GRILLEMORPION_H
