#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>
using namespace std;

#include "point.h"

class Rectangle
{
public:
  // Constructeurs
  Rectangle();
  Rectangle(double longueur, double largeur);

  //setters
  inline void SetLongueur(double longueur);
  inline void SetLargeur(double largeur);
  //getters
  inline double GetLongueur() const;
  inline double GetLargeur() const;

  // Autres méthodes
  double Perimetre() const;
  double Surface() const;
  bool ComparePerimetre(Rectangle r) const;
  bool CompareSurface(Rectangle r) const ;
  void Afficher() const;

private:
  double m_longueur,m_largeur;
  struct Point m_coin; //coin supérieur gauche
};

#endif // RECTANGLE_H
