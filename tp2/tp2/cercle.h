#ifndef CERCLE_H
#define CERCLE_H

#include <iostream>
using namespace std;

#include "point.h"

class Cercle
{
public:
  // Constructeurs
  Cercle();
  Cercle(struct Point centre, int diametre);

  //setters
  inline void SetCentre(struct Point centre);
  inline void SetDiametre(int diametre);
  //getters
  inline struct Point GetCentre() const;
  inline int GetDiametre() const;

  // Autres méthodes
  double Perimetre() const;
  double Surface() const;
  bool PtSurCercle(struct Point a) const;
  bool PtInterieurCercle(struct Point a) const;
  void Afficher() const;


private:
  struct Point m_centre;
  int m_diametre;
};

#endif // CERCLE_H
