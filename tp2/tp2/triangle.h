#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <iostream>
using namespace std;

#include "point.h"

class Triangle
{
public:
  // Constructeurs
  Triangle();
  Triangle(struct Point a, struct Point b, struct Point c);

  //setters
  inline void SetA(struct Point a);
  inline void SetB(struct Point b);
  inline void SetC(struct Point c);
  //getters
  inline struct Point GetA() const;
  inline struct Point GetB() const;
  inline struct Point GetC() const;

  // Autres méthodes
  double Distance(struct Point a, struct Point b) const;
  double Base() const;
  double Hauteur() const;
  double Surface() const;
  double Longueurs() const;
  bool Isocele() const;
  bool Rectangle() const;
  bool Equilateral() const;
  void Afficher() const;

private:
  struct Point m_a, m_b, m_c;
};

#endif // TRIANGLE_H
