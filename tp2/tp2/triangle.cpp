#include <iostream>
#include <math.h>

#include "point.h"
#include "triangle.h"

using namespace std;

Triangle::Triangle()
{}

Triangle::Triangle(struct Point a, struct Point b, struct Point c) : m_a(a), m_b(b), m_c(c)
{}

void Triangle::SetA(struct Point a)
{
    this->m_a = a;
}

void Triangle::SetB(struct Point b)
{
    this->m_b = b;
}

void Triangle::SetC(struct Point c)
{
    this->m_c = c;
}

struct Point Triangle::GetA() const
{
    return this->m_a;
}

struct Point Triangle::GetB() const
{
    return this->m_b;
}

struct Point Triangle::GetC() const
{
    return this->m_c;
}

double Triangle::Distance(struct Point a, struct Point b) const
{
    double distance;
    distance = sqrt( pow(a.x - b.x, 2) + pow(a.y - b.y, 2) );
    return distance;
}

double Triangle::Base() const
{
    double base;

    double a = this->Distance(this->m_a,this->m_b);
    double b = this->Distance(this->m_b,this->m_c);
    double c = this->Distance(this->m_a,this->m_c);

    if (a < b) {
        base = a;
    }
    else {
        base = b;
    }

    if (base < c) {
        base = c;
    }

    return base;
}

double Triangle::Hauteur() const
{
    return (2*this->Surface()) / this->Base();
}

double Triangle::Surface() const
{
    double a = Distance(this->m_a, this->m_b);
    double b = Distance(this->m_b, this->m_c);
    double c = Distance(this->m_a, this->m_c);
    double p = (a + b + c) /2;
    return sqrt(p* (p-a) * (p-b) * (p-c));
}

double Triangle::Longueurs() const
{
    return Distance(this->m_a, this->m_b) + Distance(this->m_b, this->m_c) + Distance(this->m_a, this->m_c);
}

bool Triangle::Isocele() const
{
    bool iso = false;
    if ((Distance(this->m_a, this->m_b) == Distance(this->m_b, this->m_c))
            || (Distance(this->m_b, this->m_c) == Distance(this->m_a, this->m_c))
            || (Distance(this->m_a, this->m_b) == Distance(this->m_a, this->m_c)))
    {
       iso = true;
    }
    return iso;
}

bool Triangle::Rectangle() const
{
    bool rec = false;
    if (pow(Distance(this->m_b, this->m_c),2) == pow(Distance(this->m_a, this->m_b),2) + pow(Distance(this->m_a, this->m_c),2))
    {
        rec = true;
    }
    return rec;
}

bool Triangle::Equilateral() const
{
    bool equi = false;
    if (Distance(this->m_a, this->m_b) == Distance(this->m_b, this->m_c) == Distance(this->m_a, this->m_b))
    {
        equi = true;
    }
    return equi;
}

void Triangle::Afficher() const
{
    cout << "Voici un triangle : " << this->m_a.x << this->m_a.y << this->m_b.x << this->m_b.y << this->m_c.x << this->m_c.y << endl;
}
