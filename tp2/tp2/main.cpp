#include <iostream>

#include "point.h"
#include "rectangle.h"

using namespace std;

int main()
{
    Rectangle r1, r2;
    double longueur, largeur;

    cout << "Saisissez une longueur" << endl;
    cin >> longueur;
    r1.setLongueur(longueur);
    cout << endl;

    cout << "Saisissez une largeur" << endl;
    cin >> largeur;
    r1.setLargeur(largeur);
    cout << endl;

    cout << "Prérimètre du rectangle " << endl;
    cout << r1.perimetre() << endl;

    cout << "Surface du rectangle "<< endl;
    cout << r1.surface() << endl;

    cout << "Comparons les périmètres de deux Rectangles" <<endl;
    cout << r1.comparePerimetre(r2) << endl;

    cout << "Comparons les surfaces de deux Rectangles" <<endl;
    cout << r1.compareSurface(r2) << endl;

    return 0;
}
