#include <iostream>

#include "point.h"
#include "rectangle.h"

using namespace std;

Rectangle::Rectangle() : m_longueur(0), m_largeur(0)
{}

Rectangle::Rectangle(double longueur, double largeur) : m_longueur(longueur), m_largeur(largeur)
{}

void Rectangle::SetLongueur(double longueur)
{
    this->m_longueur = longueur;
}

void Rectangle::SetLargeur(double largeur)
{
    this->m_largeur = largeur;
}

double Rectangle::GetLongueur() const
{
    return this->m_longueur;
}

double Rectangle::GetLargeur() const
{
    return this->m_largeur;
}

double Rectangle::Perimetre() const
{
    return (m_longueur + m_largeur)*2;;
}

double Rectangle::Surface() const
{
    return m_longueur*m_largeur;;
}

bool Rectangle::ComparePerimetre(Rectangle r) const
{
    bool valide = false;
    double peri = this->Perimetre();
    if (peri > r.Perimetre()){
        cout << "Le perimètre du Rectangle 1 est plus grand que le périmètre du rectangle 2" << endl;
        valide = true;
    }
    else {
        cout << "Le perimètre du Rectangle 1 est plus petit que le périmètre du rectangle 2" << endl;
        valide = false;
    }

    return valide;
}

bool Rectangle::CompareSurface(Rectangle r) const
{
    bool valide = false;
    double surf = this->Surface();
    if (surf > r.Surface()){
        cout << "La surface du Rectangle 1 est plus grande que la surface du rectangle 2" << endl;
        valide = true;
    }
    else {
        cout << "La surface du Rectangle 1 est plus petite que la surface du rectangle 2" << endl;
        valide = false;
    }

    return valide;
}

void Rectangle::Afficher() const
{
    cout << "Voici un rectangle : " << this->m_longueur << this->m_longueur << endl;
}
