#include "cercle.h"
#include "point.h"
#include "math.h"

using namespace std;

Cercle::Cercle()
{}

Cercle::Cercle(struct Point centre, int diametre) : m_centre(centre), m_diametre(diametre)
{}

void Cercle::SetCentre(struct Point centre)
{
    this->m_centre = centre;
}

void Cercle::SetDiametre(int diametre)
{
    this->m_diametre = diametre;
}

struct Point Cercle::GetCentre() const
{
    return this->m_centre;
}

int Cercle::GetDiametre() const
{
    return this->m_diametre;
}

double Cercle::Perimetre() const
{
    return 2* M_PI * (this->GetDiametre()/2);
}

double Cercle::Surface() const
{
    return 2* M_PI * (pow(this->GetDiametre(), 2)/2);
}

//détermine si un point passé en paramètre est sur le cercle
bool Cercle::PtSurCercle(struct Point a) const
{
    //x abscisse du centre du cercle
    //y ordonne du centre du cercle
    return pow((a.x - this->m_centre.x),2) + pow((a.y- this->m_centre.y),2) == pow(this->GetDiametre()/2,2);
}

//détermine si un point passé en paramètre est à l’intérieur du cercle (mais pas sur le cercle)
bool Cercle::PtInterieurCercle(struct Point a) const
{
    double distance = sqrt( pow(a.x - m_centre.x, 2) + pow(a.y - m_centre.y, 2) );
    return distance < (this->GetDiametre()/2);
}

void Cercle::Afficher() const
{
    cout << "Voici un cercle :" << this->m_centre.x << this->m_centre.y << this->m_diametre << endl;
}
